'use strict';

const Hashids = require('hashids');
const hashids = new Hashids('Salt Words');

const encode = (id) => hashids.encode(id);
const decode = (words) => hashids.decode(words);

module.exports = { encode, decode };
