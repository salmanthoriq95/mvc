'use strict';

const knex = require('../../config/connection');

const getListCustomers = async () => {
  return await knex('customers');
};

module.exports = { getListCustomers };
