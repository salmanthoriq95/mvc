'use strict';

const { encode } = require('../../config/hashids');
const { getListCustomers } = require('./customers.query');

const getCustomersService = async (payload) => {
  const resultData = await getListCustomers();

  const data = [];
  resultData.forEach((eachData) => {
    data.push({
      id: encode(eachData.customer_id),
      name: eachData.name,
      phone: `${eachData.country_code} ${eachData.phone}`,
    });
  });
  console.log(data);
  return data;
};

module.exports = { getCustomersService };
