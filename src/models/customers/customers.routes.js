'use strict';

const { Router } = require('express');
const { getCustomersController } = require('./customers.controller');
const router = Router();

router.get('/', getCustomersController);
module.exports = router;
