'use strict';

const { getCustomersService } = require('./customers.service');
const { getCustomerValidate } = require('./customers.validator');

const getCustomersController = async (req, res) => {
  try {
    const validReq = getCustomerValidate(req.query);
    const serviceResult = await getCustomersService(validReq);
    return res.send({
      success: true,
      data: serviceResult,
    });
  } catch (error) {
    console.log(error);
    return res.status(400).send({
      success: false,
      Message: 'Error!',
    });
  }
};

module.exports = { getCustomersController };
