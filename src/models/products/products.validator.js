const Joi = require('joi');

const getProductsValidate = (payload) => {
  const schema = Joi.object({
    search: Joi.string().optional(),
    order: Joi.string().optional(),
  });

  const { error, value } = schema.validate({
    search: payload.search,
    order: payload.order,
  });

  if (error) {
    throw new Error();
  }

  return value;
};

module.exports = { getProductsValidate };
