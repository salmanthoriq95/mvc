'use strict';

const knex = require('../../config/connection');

const getListProducts = async () => {
  return await knex('prod');
};

module.exports = { getListProducts };
