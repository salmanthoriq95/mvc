'use strict';

const { getProductsService } = require('./products.service');
const { getProductsValidate } = require('./products.validator');

const getProductsController = async (req, res) => {
  try {
    const validReq = getProductsValidate(req.query);
    const serviceResult = await getProductsService(validReq);
    return res.send({
      success: true,
      data: serviceResult,
    });
  } catch (error) {
    console.log(error);
    return res.status(400).send({
      success: false,
      Message: 'Error!',
    });
  }
};

module.exports = { getProductsController };
