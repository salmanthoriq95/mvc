'use strict';

const { Router } = require('express');
const { getProductsController } = require('./products.controller');

const router = Router();

router.get('/', getProductsController);

module.exports = router;
