'use strict';

const { encode } = require('../../config/hashids');
const { getListProducts } = require('./products.query');

const getProductsService = async (payload) => {
  const resultData = await getListProducts();

  const data = [];
  resultData.forEach((eachData) => {
    data.push({
      id: encode(eachData.customer_id),
      name: eachData.product_name,
      price: eachData.price,
    });
  });
  console.log(data);
  return data;
};

module.exports = { getProductsService };
