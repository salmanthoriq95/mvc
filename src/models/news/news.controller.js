'use strict';

const { getNewsService } = require('./news.service');
const { getNewsValidate } = require('./news.validator');

const getNewsController = async (req, res) => {
  try {
    const validReq = getNewsValidate(req.query);
    const serviceResult = await getNewsService(validReq);
    return res.send({
      success: true,
      data: serviceResult,
    });
  } catch (error) {
    console.log(error);
    return res.status(400).send({
      success: false,
      Message: 'Error!',
    });
  }
};

module.exports = { getNewsController };
