'use strict';

const { Router } = require('express');
const { getNewsController } = require('./news.controller');
const router = Router();

router.get('/', getNewsController);

module.exports = router;
