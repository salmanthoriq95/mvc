'use strict';

const knex = require('../../config/connection');

const getListNews = async () => {
  return await knex('news');
};

module.exports = { getListNews };
