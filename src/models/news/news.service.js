'use strict';

const { encode } = require('../../config/hashids');
const { getListNews } = require('./news.query');

const getNewsService = async (payload) => {
  const resultData = await getListNews();

  const data = [];
  resultData.forEach((eachData) => {
    data.push({
      id: encode(eachData.customer_id),
      name: eachData.name,
      phone: `${eachData.country_code} ${eachData.phone}`,
    });
  });
  console.log(data);
  return data;
};

module.exports = { getNewsService };
