'use strict';

const { Router } = require('express');
const router = Router();
const customerRoutes = require('../models/customers/customers.routes');
const newsRoutes = require('../models/news/news.routes');
const productRoutes = require('../models/products/products.routes');

module.exports = {
  customers: router.use('/customers', customerRoutes),
  news: router.use('/news', newsRoutes),
  products: router.use('/products', productRoutes),
};
