'use strict';

const express = require('express');
const app = express();
const port = 3002;

const routers = require('./src/controllers');

// Routing
Object.keys(routers).forEach((key) => app.use('/', routers[key]));

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
